pipeline {
    agent any
    
    environment {
        registry = "kss7/dotnetapp"
        img = "${registry}:${env.BUILD_ID}"
        containerName = "webapp_container"
        portMapping = "8081:80" // Change the port mapping as needed
        DOCKER_CREDENTIALS = credentials('ass-02')
    }
    
    stages {
        stage('Checkout') {
            steps {
                git branch: 'main', url: 'https://gitlab.com/PraveenaN/4119-praveenan.git'
            }
        }
        
        stage('Build and Publish') {
            steps {
                script {
                    // Build and publish the Docker image
                    docker.build(img, "-f Dockerfile .")
                }
            }
        }
        
        stage('Stop and Remove Docker Container') {
            steps {
                script {
                    // Stop and remove the container only if it exists
                    sh 'docker stop ${containerName} || true'
                    sh 'docker rm ${containerName} || true'
                }
            }
        }

        stage('Run Container') {
            steps {
                script {
                    // Run the Docker container
                    sh "docker run -d --name ${containerName} -p ${portMapping} ${img}"
                }
            }
        }
    }

    post {
        success {
            script {
                // Remove the Docker image if the pipeline succeeds
                sh "docker rmi -f ${img}"
            }
        }
        
        failure {
            script {
                // Remove the Docker image if the pipeline fails
                sh "docker rmi -f ${img}"
            }
        }
    }
}
